﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Model.EF;
using System.Data.SqlClient;
using PagedList;

namespace Model.Dao
{
    public class UserDao
    {
        OnlineShopDbContext db = null;
        public UserDao()
        {
            db = new OnlineShopDbContext();
        }
        public long Insert(User entity)
        {
            db.Users.Add(entity);
            db.SaveChanges();
            return entity.ID;
        }
        public bool Update(User entity)
        {
            try {
                var user = db.Users.Find(entity.ID);
                user.Name = entity.Name;
                user.Address = entity.Address;
                user.Email = entity.Email;
                user.ModifiledBy = entity.ModifiledBy;
                user.ModifiledDate =DateTime.Now;
                db.SaveChanges();
                return true;
            }
            catch(Exception ex){
                return false;
            }
            
        }
        public IEnumerable<User> ListAllPaging(int page, int pageSize)
        {
            return db.Users.OrderByDescending(x => x.CreatedDate).ToPagedList(page,pageSize);
        }
        public User Viewdetail(int id)
        {
            return db.Users.Find(id); // Users OnlineShopDbContext
        }
        public User GetById(string userName)
        {
            return db.Users.SingleOrDefault(x=>x.UserName == userName);
        }
        public int Login(string userName, string password)
        {
            var result = db.Users.SingleOrDefault(x => x.UserName == userName);
            if (result ==null)
            {
                return 0;
            }
            else {
                if(result.Status == false)
                {
                    return -1;
                }else
                {
                    if (result.Password == password)
                    {
                        return 1;
                    }
                    else
                        return -2;
                }
            }
        }
    }
}
