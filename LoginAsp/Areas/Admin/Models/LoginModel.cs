﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LoginAsp.Areas.Admin.Models
{
    public class LoginModel
    {
        [Required(ErrorMessage = "Mời Nhập UserName")]
        public string UserName { set; get; }
        [Required(ErrorMessage = "Mời Nhập Password")]
        public string Password { set; get; }
        [Required(ErrorMessage = "Mời Nhập RememberMe")]
        public bool RememberMe { set; get; }
    }
}
